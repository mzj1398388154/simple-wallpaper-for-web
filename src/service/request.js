import axios from 'axios';
import { BASE_URL } from '../utils/constants';

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 5000,
  withCredentials: true,
});

export function get(url, params = {}) {
  return new Promise((resolve, reject) => {
    axiosInstance
      .get(url, { params })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export function post(url, data = {}) {
  return new Promise((resolve, reject) => {
    axiosInstance
      .post(url, data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
