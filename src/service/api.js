import { get } from "./request";

export const getImageData = params => get('HPImageArchive.aspx', params);
